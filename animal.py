class LivingThing:
    pass

class Mammal(LivingThing):
    def __init__(self, name):
        self.name = name    

class Human(Mammal):
    "this is a class to create a person"
    
    def __init__(self, name):
        self.name = name
    

    def speak(self):
        "This is how we make a person talk"
        print("Hello!")
 
