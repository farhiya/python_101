from factory_functions import *
import unittest


class TestBakingFunctions(unittest.TestCase):                                                                      

    def test_dough(self):
        self.assertEqual(make_dough_option('flour', 'water'), 'dough')

    def test__dough_wrong_ingerdients(self):
        self.assertEqual(make_dough_option('cement', 'water'), 'not dough')

    def test_dough_right_ingerdients_different_order(self):
        self.assertEqual(make_dough_option('water', 'flour'), 'dough')

    def test_dough_ingredients_in_name_but_not_alone(self):
        self.assertEqual(make_dough_option('flourman', 'topwater'), 'not dough')
    
    def test_dough_same_ingredient(self):
        self.assertEqual(make_dough_option('flour', 'flour'), 'not dough')
    
    def test_get_pao(self):
        self.assertEqual(bake_option('dough'), 'pao')
    
    def test_not_pao(self):
        self.assertEqual(bake_option('jam'), 'not pao')
    
    def test_whole_factory_works(self):
        self.assertEqual(run_factory('flour', 'water'),'pao')

    def test_whole_factory_wrong_ingerdeients(self):
        self.assertEqual(run_factory('coke', 'mentoes'),'not pao')

    def test_whole_factory_works_ingredients_swapped(self):
        self.assertEqual(run_factory('water', 'flour'),'pao')

    def test_whole_factory_test_ingredients_in_name_but_not_alone(self):
        self.assertNotEqual(run_factory('flourman', 'topwater'),'pao')

    def test_whole_factory_same_ingredient(self):
        self.assertNotEqual(run_factory('flour', 'flour'), 'pao')
    

if __name__ == '__main__':
    unittest.main()
  
# #User Story 1 AS a bread maker, I want to provide flour and water to my 
# #make_dough_option and get out dough, else I want not dough. 
# #So that I can then bake the bread.
# print("test 1a", make_dough_option('flour', 'water') == 'dough')
# print("test 1b", make_dough_option('cement', 'water') == 'not dough')
# print("test 1c", make_dough_option('water', 'flour') == 'dough')
# print("test 1d", make_dough_option('flourman', 'topwater') == 'not dough')
# print("test 1e", make_dough_option('flour', 'flour') == 'not dough')


# #User Story 2 AS a bread maker, I want to be able able take dough and 
# #user the bake_option to get Pao. Else I should get not Pao. 
# #So That I can make bread.
# print("test 2a", bake_option('dough') == 'pao')
# print("test 2b", bake_option('cement') == 'not pao')



# #User Story 3 As a break maker, I want an option of run_factory that will 
# #take in flour and water and give me Pao, else give me not Pao. 
# #So I can make break with one simple action.
# print("test 3a", run_factory('flour', 'water') == 'pao')
# print("test 3b", run_factory('toothpaste', 'water') == 'not pao')
# print("test 3c", run_factory('water', 'flour') == 'pao')
# print("test 3d", run_factory('flourman', 'topwater') == 'not pao')
# print("test 3e", run_factory('flour', 'flour') == 'not pao')