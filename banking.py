class Account:
    def __init__(self):
        self.balance = 0
        self.overdraft_limit = 0

    def deposit(self, amount):
        self.balance += amount

    def withdraw(self, amount):
        if amount > self.balance:
            raise ValueError("account overdrawn! Unable to withdraw")
        self.balance -= amount

    def pay_interest(self,amount):
        self.balance *= (1 + amount)

    def transfer(self,other,amount):
        self.withdraw(amount)
        other.deposit(amount)
        



barclays = Account()
halifax = Account()
print(f"My balance is {barclays.balance}")

barclays.deposit(100)
print(f"My balance is {barclays.balance}")

barclays.deposit(100)
print(f"My balance is {barclays.balance}")

barclays.pay_interest(3/100)
print(f"My balance is {barclays.balance}")

try :
    withdraw_amount = int(input("how much you want? "))
    barclays.withdraw(withdraw_amount)
    # print(name)
except NameError:
    print("name error")
else:
    print(f"My balance is {barclays.balance}")


