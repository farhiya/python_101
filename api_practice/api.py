import json
import requests
import sys
# file = open('bored-api.txt')
# response = file.read()

response= requests.get('https://randomuser.me/api')

if response.status_code != 200:
    print("something went wrong")
    # sys.exit()
else:
    print("Got status 200 from the API")


print("This is my txt file:\n")
print(response.status_code)

parsed_response = json.loads(response.text)


print("This is my parsed response:\n")
print(parsed_response['results'][0]['name']['last'])


# print(parsed_response)
# # print(len(parsed_response))
# # print(type(parsed_response))
# print(parsed_response['activity'])
# print(parsed_response['type'])